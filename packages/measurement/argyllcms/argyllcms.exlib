# Copyright 2013-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=h${PNV}

SUMMARY="ICC compatible color management system (autotools version)"
HOMEPAGE="https://github.com/hughsie/hargyllcms"
DOWNLOADS="https://people.freedesktop.org/~hughsient/releases/${MY_PNV}.tar.xz"

DESCRIPTION="
The Argyll color management system supports accurate ICC profile creation for
acquisition devices, CMYK printers, film recorders and calibration and profiling
of displays.

Spectral sample data is supported, allowing a selection of illuminants observer
types, and paper fluorescent whitener additive compensation. Profiles can also
incorporate source specific gamut mappings for perceptual and saturation
intents. Gamut mapping and profile linking uses the CIECAM02 appearance model,
a unique gamut mapping algorithm, and a wide selection of rendering intents. It
also includes code for the fastest portable 8 bit raster color conversion
engine available anywhere, as well as support for fast, fully accurate 16 bit
conversion. Device color gamuts can also be viewed and compared using a VRML
viewer.
"

LICENCES="AGPL-3 GPL-3 MIT"
SLOT="0"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/libusb:1
        media-libs/libpng:=
        media-libs/tiff
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXinerama
        x11-libs/libXrandr
        x11-libs/libXxf86vm
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    recommendation:
        sys-apps/colord [[
            description = [ Installs udev rules required for using measurement devices ]
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)

WORK=${WORKBASE}/${MY_PNV}

