# Copyright 2016 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm; then
    SCM_REPOSITORY="https://git.kernel.org/pub/scm/network/wireless/iwd.git"
    SCM_ell_REPOSITORY="https://git.kernel.org/pub/scm/libs/ell/ell.git"
    SCM_ell_UNPACK_TO="${WORKBASE}/ell"
    SCM_SECONDARY_REPOSITORIES="ell"
    require scm-git autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
    DOWNLOADS=""
else
    DOWNLOADS="mirror://kernel/linux/network/wireless/${PNV}.tar.xz"
fi

require systemd-service

export_exlib_phases src_install

SUMMARY="Wireless daemon for Linux"
HOMEPAGE="https://git.kernel.org/cgit/network/wireless/${PN}.git/"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/docutils
    build+run:
        sys-apps/dbus
"

BUGS_TO="keruspe@exherbo.org"

if ever at_least scm ; then
    :
else
    DEPENDENCIES+="
        build+run:
            dev-libs/ell[>=0.31]
    "

    DEFAULT_SRC_CONFIGURE_PARAMS+=( --enable-external-ell )
fi

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    --localstatedir=/var
    --enable-debug
    --enable-optimization
    --enable-pie
    --enable-manual-pages
    --with-systemd-unitdir="${SYSTEMDSYSTEMUNITDIR}"
    --with-systemd-modloaddir="/usr/$(exhost --target)/lib/modules-load.d"
    --with-systemd-networkdir="/usr/$(exhost --target)/lib/systemd/network"
)


# Some tests are failing because they require unmerged commits into the Linux tree
# and the kernel needs some options to be enabled (the same one would enable to make iwd run)
# which aren't on the CI (yet?)
RESTRICT="test"

iwd_src_install() {
    default
    keepdir /var/lib/${PN}
}

